$(document).ready(function() {
	setInterval(getUser, 1000);
	setInterval(getLoad, 1000);
    setInterval(getNet, 1000);
    setInterval(currentTime,1000);
});

function currentTime() {
  document.getElementById('time').innerHTML =
  "Current Time:"+ getDateAndTime();
  var t = setTimeout(startTime, 500);
}


function checkTime(i) {
  if (i < 10) {i = "0" + i};  
  return i;
}


function getUser() {
	a = $.ajax({
		url: 'http://ceclnx01.cec.miamioh.edu/~campbest/cse383-f19-campbest-public/ajax1/info.php/api/v1/who',
		method: "GET"
	}).done(function(data) {
		//clear out old data
		$("#users").html("");
		 len = data.who.length;
		for (i = 0; i < len; i++) {
			$("#users").append("<tr><td>" + data.who[i].uid + "</td><td>" + data.who[i].ip + "</td></tr>");
		}
	}).fail(function(error) {
		logError("Who");
	});
}


function getDateAndTime() {
	var today = new Date();
	var month = today.getMonth()+1;
	var date = today.getDate();
	var year =today.getFullYear();
	var date = month+'/'+date+'/'+year;
	var minutes = checkTime(today.getMinutes());
	var seconds = checkTime(today.getSeconds());
	var time = today.getHours() + ":" + minutes + ":" + seconds;
	var dateTime = date+' '+time;
	return dateTime;
}

function logError(error) {
	var newItem = document.createElement("li");
	var dateTime = getDateAndTime();
	var textNode = document.createTextNode(dateTime + " Error getting " + error);
	newItem.appendChild(textNode);
	var list = document.getElementById("log").getElementsByTagName("ul")[0];
	list.insertBefore(newItem, list.childNodes[0]);

}

function getLoad() {
	a = $.ajax({
		url: 'http://ceclnx01.cec.miamioh.edu/~campbest/cse383-f19-campbest-public/ajax1/info.php/api/v1/loadavg',
		method: "GET"
	}).done(function(data) {
		//clear out old data
		var oneMin = document.getElementById("onemin");
		var fiveMin = document.getElementById("fivemin");
		var fifteenMin = document.getElementById("fifteenmin");
		var numRunning = document.getElementById("numRunning");
		var ttlProc = document.getElementById("ttlProc");
		oneMin.innerHTML = data.loadavg.OneMinAvg;
		fiveMin.innerHTML = data.loadavg.FiveMinAvg;
		fifteenMin.innerHTML = data.loadavg.FifteenMinAvg;
		numRunning.innerHTML = data.loadavg.NumRunning;
		ttlProc.innerHTML = data.loadavg.TtlProcesses;
	}).fail(function(error) {
		logError("LoadAvg");
	});
}

var lastTx = -1;
var lastRx = -1;

function getNet() {
	a = $.ajax({
		url: 'http://ceclnx01.cec.miamioh.edu/~campbest/cse383-f19-campbest-public/ajax1/info.php/api/v1/network',
		method: "GET"
	}).done(function(data) {
		//clear out old data
		var txdata = data.network.txbytes;
		var rxdata = data.network.rxbytes;
		if (lastTx <= 0) lastTx = txdata;
		if (lastRx <= 0) lastRx = txdata;
		var avgtx = txdata - lastTx;
		var avgrx = rxdata - lastRx;
		var txbytesOutput = document.getElementById("txbytes");
		var rxbytesOutput = document.getElementById("rxbytes");
		var txavg = document.getElementById("txavg");
		var rxavg = document.getElementById("rxavg");
		txbytesOutput.innerHTML = data.network.txbytes;
		rxbytesOutput.innerHTML = data.network.rxbytes;
		txavg.innerHTML = avgtx;
		rxavg.innerHTML = avgrx;
		lastTx = txdata;
		lastRx = rxdata;

	}).fail(function(error) {
		logError("Network");
	});
}

